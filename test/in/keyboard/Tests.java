package in.keyboard;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class Tests {
    
    private static PipedOutputStream toIn;
    
    @BeforeAll
    static void initSystemIn() throws IOException {
        toIn = new PipedOutputStream();
        System.setIn(new PipedInputStream(toIn));
    }
    
    @BeforeEach
    void cleanInputStream() throws IOException {
        String end = "<<<<<<end";
        // purge System.in;
        toIn.write("\n".getBytes());
        toIn.write(end.getBytes());
        toIn.write("\n".getBytes());
        toIn.flush();       
        while(! end.equals(Keyboard.getString()));
    }
    
    @Test
    void testGetChar() throws IOException {
        char expected = 'A';
        toIn.write("ABCDE\n".getBytes());
        
        char actual = Keyboard.getChar();        
        assertEquals(expected, actual);
    }
    
    @Test
    void testGetString() throws IOException {
        String expected = "AB\tCDE";
         toIn.write("AB\tCDE\nfghij\n".getBytes());

        String actual = Keyboard.getString();        
        assertEquals(expected, actual);
    }
    
    @Test
    void testGetChars() throws IOException {
        String expected = "AB\tCDE";
        toIn.write("AB\tCDE\nfghij\n".getBytes());
        
        char[] actual = Keyboard.getChars();        
        assertArrayEquals(expected.toCharArray(), actual);
    }
    
    @Test
    void testGetIntOk() throws IOException {
        int expected = 12345;
        toIn.write("12345\nabcde\n".getBytes());
        
        int actual = Keyboard.getInt();        
        assertEquals(expected, actual);
    }
    
    @Test
    void testGetIntKo() throws IOException {
        int expected = -1;
        toIn.write("12345 xyz\nabcde\n".getBytes());
       
        int actual = Keyboard.getInt();        
        assertEquals(expected, actual);
    }
    
    @Test
    void testGetDoubleOk() throws IOException {
        double expected = 12345.5678;
        toIn.write("12345.5678\nabcde\n".getBytes());
        
        double actual = Keyboard.getDouble();        
        assertEquals(expected, actual);
    }
    
    @Test
    void testGetDoubleKo() throws IOException {
        double expected = Double.NaN;
        toIn.write("12345 xyz\nabcde\n".getBytes());
        
        double actual = Keyboard.getDouble();        
        assertEquals(expected, actual);
    }
}
