/*
 * Created on 29 nov. 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package in.keyboard;

import java.io.*;

/**
 * Lire sur l’entrée standard.
 *
 * On peut lire des informations sur l’entrée standard du programme.
 * Pour cela, les primitives Java sont dans la classe System.in. 
 * Mais elles sont d’un niveau trop bas et un peu compliqué. 
 * Nous présentons donc la classe Keyboard (ne faisant donc pas partie 
 * de Java) qui permettra de faire des entrées avec des fonctions plus simples.
 * 
 * NB : ces méthodes sont minimales et ne sont réutilisables que pour débuter la
 * programmation des entrées-sorties en Java. Elles devront être améliorées
 * ensuite selon les besoins du programmeur.
 * 
 * @see System#in
 */

public class Keyboard {
    private static BufferedReader keyboard = new BufferedReader(new InputStreamReader(System.in));

    // toutes les méthodes sont des méthodes de classe (static)
    /**
     * La méthode getChar() retourne un caractère lu sur l’entrée standard. 
     * Elle retourne le premier caractère tapé jusqu’au retour chariot.
     * 
     * @return le caractère lu
     */
    public static char getChar() {
        return getString().charAt(0);
    }

    /**
     * La méthode getChars() retourne un tableau de caractères lus sur l’entrée standard. 
     * Elle retourne les caractères tapés jusqu’au retour chariot. 
     * Exemple d'utilisation : 
     * <pre>{@code    System.out.print("entrez un code : "); 
     *    char[] tab = Keyboard.getChars();}</pre>
     * @return le tableau des caractères lus
     */
    public static char[] getChars() {
        String s = getString();
        return s.toCharArray();
    }

    /**
     * La méthode getString() retourne la chaîne de caractères lue sur l’entrée standard. 
     * Elle retourne tous les caractères tapés jusqu’au retour chariot.
     * Exemple d'utilisation : 
     * <pre>{@code     System.out.print("votre nom : "); 
     *     String s = Keyboard.getString();}</pre>
     * @return la chaîne de caractères lue
     */
    public static String getString() {
        return getLine();
    }

    /**
     * La méthode getInt() est analogue à getString, mais convertit la chaîne lue
     * sur l’entrée standard en un entier qu’elle retourne.
     * 
     * @return l'entier lu
     */
    public static int getInt() {
        try {
            return Integer.parseInt(getLine());
        } catch (NumberFormatException e) {
            return -1;
        }
    }

    /**
     * La méthode getDouble() est analogue à getString, mais convertit la chaîne lue
     * sur l’entrée standard en un réél de type double qu’elle retourne.
     * 
     * @return le réél lu
     */
    public static double getDouble() {
        try {
            return Double.parseDouble(getLine());
        } catch (NumberFormatException e) {
            return Double.NaN;
        }
    }

    /**
     * La méthode pause() attend un retour chariot sur l’entrée standard. 
     * Elle est utile pour mettre un point d’arrêt dans le programme. 
     * On peut ainsi éviter que la fenêtre d’exécution du programme s’en 
     * aille trop vite en plaçant un {@code pause()} à la fin du programme.
     */
    public static void pause() {
        try {
            keyboard.read();
        } catch (IOException e) {
        }
    }

    private static String getLine() {
        String line;
        try {
            line = keyboard.readLine();
        } catch (IOException e) {
            return "";
        }
        return line;
    }
}
