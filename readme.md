# La classe `Keyboard` ou la saisie en Java à la porté de tous

Miguel dans sa grande bonté a décidé de simplifier, pour étudiants, la saisie 
d'informations via le flux d'entrée standard (le clavier quoi...) qui, comme chacun le sait,
n'est pas si facile en Java, surtout quand on débute.

À cet effet il a écrit la classe [Keyboard](./src/in/keyboard/Keyboard.java) 
qui fournit des méthodes pour lire facilement des données (char, int, String, ...) 
sur le flux d'entrée standard.

## Construction du projet 

Pour construire le sujet du TP utiliser la commande :  
`./gradlew build`  
le classe Keyboard sera alors compilée et des archives jar contenant la librairie, 
les sources et la javadoc seront alors disponible dans le dossier **build/libs/**.

## Importation du projet dans votre IDE favori (Eclipse/IntelliJ/...)

Il est possible d'importer le projet dans votre IDE favori.
Pour cela importer le projet comme un *projet gradle* et, avec un peu de chance, cela devrait bien se passer... 
